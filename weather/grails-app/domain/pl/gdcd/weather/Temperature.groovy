package pl.gdcd.weather

class Temperature {

	String unit = "C"
	
    Double temperature
    String location
    
	public Temperature(Double temperature, String location) {
		this.temperature = temperature
		this.location = location
	}
}
