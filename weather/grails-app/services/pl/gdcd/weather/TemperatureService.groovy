package pl.gdcd.weather

import grails.transaction.Transactional

class TemperatureService {
	
	boolean transactional = false
	
	def openWeatherMapTemperatureService
	
    def getCurrent(def location) {
		// TODO: more providers, average the temperature from them
		return openWeatherMapTemperatureService.getCurrent(location)
	}
}
