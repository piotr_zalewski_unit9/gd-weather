package pl.gdcd.weather.provider

import grails.plugins.rest.client.RestBuilder
import grails.transaction.Transactional
import pl.gdcd.weather.Temperature
import pl.gdcd.weather.exceptions.LocationNotFoundException

class OpenWeatherMapTemperatureService {
	
	boolean transactional = false
	
	// TODO: this should not be hardcoded
	def baseUrl = 'http://api.openweathermap.org/data/2.5/weather'
	def apiKey = '9b372bae2b86601d28e87d08082619cc'
	
    def getCurrent(def location) {
		def rest = new RestBuilder()
		
		def response = rest.get(baseUrl + '?units=metric&q=' + location + '&APPID=' + apiKey)
		
		if (response.status == 200) {
			if (response.json.cod.toString() == "200" ) {
				def tempFloat = response.json.main.temp
				def locationCountry = response.json.sys.country
				def locationCity = response.json.name
				return new Temperature(tempFloat, locationCity + ',' + locationCountry)
			} else if (response.json.cod.toString() == "404") {
				throw new LocationNotFoundException()
			}
		} else {
			throw new Exception('Error in provider response.')
		}
    }
}
