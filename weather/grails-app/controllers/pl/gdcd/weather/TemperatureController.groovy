package pl.gdcd.weather

import pl.gdcd.weather.exceptions.LocationNotFoundException;
import grails.converters.JSON

class TemperatureController {
	
	//response format avoid the error that views are not found
	static responseFormats = ['json']

	static allowedMethods = [index:"GET"]
	
	def temperatureService
	
    def index() {
		def location = params.location
		log.debug('Location="' + location + '"')
		
		if (!location) {
			cache false		
			render(status: 400, text: 'Location parameter missing.')
			return
		}
		
		try {
			def temp = temperatureService.getCurrent(location)
			cache shared:true, validFor: 3600  // 1hr
			render temp.part(except:['id','class']) as JSON
		} catch (LocationNotFoundException e1) {
			cache false
			render(status: 404, text: 'Location not found.')
			return
		} catch (Exception e) {
			cache false
			render(status: 500)
			return
		}
	}
	
}
