import grails.util.Environment 

// If you have another plugin that enables functional tests,
// such as Geb, you won't need to do this
if(Environment.current == Environment.TEST) {
	eventAllTestsStart = {
		if (getBinding().variables.containsKey("functionalTests")) {
			functionalTests << "functional"
		}
	}
}