package pl.gdcd.weather

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import grails.plugins.rest.client.RestBuilder

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
class TemperatureControllerTestSpec extends Specification {

	// FIXME: This should not be hardcoded
	def baseUrl = 'http://localhost:8080/weather/temperature/'
	
	void "Test that a correct GET request returns 200 response code"() {
		given:
		def rest = new RestBuilder()
		
	    when:
		def response = rest.get(baseUrl + "Poznan,Poland")
	
	    then:
	    assert response.status == 200
	}
	
	void "Test that a GET request with missing parameters results with 400 response code"() {
		given:
		def rest = new RestBuilder()
		
	    when:
		def response = rest.get(baseUrl + "")
	
	    then:
	    assert response.status == 400
	}
	
	void "Test that a request with method POST results in a 405 response code"() {
		given:
		def rest = new RestBuilder()
		
	    when:
		def response = rest.post(baseUrl + "Poznan,Poland")
	
	    then:
	    assert response.status == 405
	}
	
	void "Test that a request with method PUT results in a 405 response code"() {
		given:
		def rest = new RestBuilder()
		
		when:
		def response = rest.put(baseUrl + "Poznan,Poland")
	
		then:
		assert response.status == 405
	}
	
	void "Test that a request with method DELETE results in a 405 response code"() {
		given:
		def rest = new RestBuilder()
		
		when:
		def response = rest.delete(baseUrl + "Poznan,Poland")
	
		then:
		assert response.status == 405
	}
}
