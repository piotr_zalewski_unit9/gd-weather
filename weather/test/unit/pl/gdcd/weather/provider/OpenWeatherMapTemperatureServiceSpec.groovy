package pl.gdcd.weather.provider

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(OpenWeatherMapTemperatureService)
class OpenWeatherMapTemperatureServiceSpec extends Specification {
	
	def location = 'Poznan,Poland'
	
	// TODO: this should use mock
    void "Test that the service returns a correct temperature for a correct location query"() {
		given:
		service
		
	    when:
		def temp = service.getCurrent(location)
	
	    then:
		assertNotNull temp
	    assertNotNull temp.temperature
		assert temp.temperature > -45
		assert temp.temperature < 45
	}
}
